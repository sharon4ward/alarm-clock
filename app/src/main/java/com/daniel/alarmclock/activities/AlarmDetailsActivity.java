package com.daniel.alarmclock.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;

import com.daniel.alarmclock.R;
import com.daniel.alarmclock.data.AlarmsDBAdapter;
import com.daniel.alarmclock.models.Alarm;
import com.daniel.alarmclock.models.Time;
import com.daniel.alarmclock.utils.MyInputFilter;

import static com.daniel.alarmclock.utils.KeyboardUtils.hideSoftKeyboard;

public class AlarmDetailsActivity extends AppCompatActivity {


    EditText alarmTimeEdit;
    Alarm alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_details_activity);

        // Autohide keyboard when clicking on fields other than edit control
        setupUI(findViewById(R.id.idADDetailsParent));

        // This function force the user to enter just numbers in the time field.
        Bundle data = getIntent().getExtras();
        try {
            alarm = (Alarm) data.getParcelable("ALARM");

            EditText alarmTitle = (EditText) findViewById(R.id.idADAlarmTitle);
            EditText alarmTime = (EditText) findViewById(R.id.idADTime);
            Switch active = (Switch) findViewById(R.id.idADActive);
            CheckBox repeat = (CheckBox) findViewById(R.id.idADRepeat);
            CheckBox sunday = (CheckBox) findViewById(R.id.idADSunday);
            CheckBox monday = (CheckBox) findViewById(R.id.idADMonday);
            CheckBox tuesday = (CheckBox) findViewById(R.id.idADTuesday);
            CheckBox wednesday = (CheckBox) findViewById(R.id.idADWednesday);
            CheckBox thursday = (CheckBox) findViewById(R.id.idADThursday);
            CheckBox friday = (CheckBox) findViewById(R.id.idADFriday);
            CheckBox saturday = (CheckBox) findViewById(R.id.idADSaturday);
            alarmTitle.setText(alarm.getTitle());
            Time time = new Time(alarm.getHour(), alarm.getMinute());
            alarmTime.setText(time.getStringTime());

            active.setChecked(alarm.isActive());
            repeat.setChecked(alarm.isRepeat());
            sunday.setChecked(alarm.isSunday());
            monday.setChecked(alarm.isMonday());
            tuesday.setChecked(alarm.isTuesday());
            wednesday.setChecked(alarm.isWednesday());
            thursday.setChecked(alarm.isThursday());
            friday.setChecked(alarm.isFriday());
            saturday.setChecked(alarm.isSaturday());

        } catch (NullPointerException exp) {
            // do nothing
        }

        InputFilter filter = new MyInputFilter();
        alarmTimeEdit = (EditText) findViewById(R.id.idADTime);
        alarmTimeEdit.setFilters(new InputFilter[]{filter});

    }

    public void onDeleteClick(View view) {
        if( alarm != null ) {
            AlarmsDBAdapter dbAdapter = new AlarmsDBAdapter(this);
            dbAdapter.delete(alarm.getAlarmId());

            Intent alarmListIntent = new Intent(this, AlarmsListActivity.class);
            startActivity(alarmListIntent);

        }

    }


    public void onSaveClick(View view) {
        boolean error = false;
        String alarmTitle = ((EditText) findViewById(R.id.idADAlarmTitle)).getText().toString();
        String alarmTime = ((EditText) findViewById(R.id.idADTime)).getText().toString();
        Time time = new Time(alarmTime);
        if (!error) {
            boolean active = ((Switch) findViewById(R.id.idADActive)).isChecked();
            boolean repeat = ((CheckBox) findViewById(R.id.idADRepeat)).isChecked();
            boolean sunday = ((CheckBox) findViewById(R.id.idADSunday)).isChecked();
            boolean monday = ((CheckBox) findViewById(R.id.idADMonday)).isChecked();
            boolean tuesday = ((CheckBox) findViewById(R.id.idADTuesday)).isChecked();
            boolean wednesday = ((CheckBox) findViewById(R.id.idADWednesday)).isChecked();
            boolean thursday = ((CheckBox) findViewById(R.id.idADThursday)).isChecked();
            boolean friday = ((CheckBox) findViewById(R.id.idADFriday)).isChecked();
            boolean saturday = ((CheckBox) findViewById(R.id.idADSaturday)).isChecked();
            if (alarm != null) {
                alarm = new Alarm(
                        alarm.getAlarmId(),
                        time.getHour(),
                        time.getMinutes(),
                        alarmTitle,
                        repeat,
                        sunday,
                        monday,
                        tuesday,
                        wednesday,
                        thursday,
                        friday,
                        saturday,
                        active
                );
                AlarmsDBAdapter dbAdapter = new AlarmsDBAdapter(this);
                dbAdapter.update(alarm.getAlarmId(), alarm.getContentValues());

                Intent alarmListIntent = new Intent(this, AlarmsListActivity.class);
                startActivity(alarmListIntent);
            } else {
                alarm = new Alarm(
                        0,
                        time.getHour(),
                        time.getMinutes(),
                        alarmTitle,
                        repeat,
                        sunday,
                        monday,
                        tuesday,
                        wednesday,
                        thursday,
                        friday,
                        saturday,
                        active
                );

                AlarmsDBAdapter dbAdapter = new AlarmsDBAdapter(this);
                dbAdapter.insert(alarm.getContentValues());

                alarm.schedule(this.getApplicationContext());

                Intent alarmListIntent = new Intent(this, AlarmsListActivity.class);
                startActivity(alarmListIntent);
            }

        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(AlarmDetailsActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

}