package com.daniel.alarmclock.activities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.daniel.alarmclock.R;
import com.daniel.alarmclock.models.Alarm;
import com.daniel.alarmclock.service.AlarmService;

import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RingActivity extends AppCompatActivity {
    Button dismiss;
    ImageView clock;
    TextView question;
    EditText editAnswer;

    int one = 0, two = 0;
    int answer = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ring);

        question = (TextView) findViewById(R.id.idARQuestionText);
        editAnswer = (EditText) findViewById(R.id.idARAnsweer);

        generateQuestion();

        dismiss = (Button) findViewById(R.id.activity_ring_dismiss);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int answerVal = Integer.parseInt(editAnswer.getText().toString());

                if( answer == answerVal ) {
                    Intent intentService = new Intent(getApplicationContext(), AlarmService.class);
                    getApplicationContext().stopService(intentService);
                    finish();
                } else {
                    generateQuestion();
                }
            }
        });

        animateClock();
    }

    private void generateQuestion() {
        one = ThreadLocalRandom.current().nextInt(0, 50);
        two = ThreadLocalRandom.current().nextInt(0, 50);
        question.setText(String.format("%d + %d", one, two));
        answer = one + two;
    }
    private void animateClock() {
        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(clock, "rotation", 0f, 20f, 0f, -20f, 0f);
        rotateAnimation.setRepeatCount(ValueAnimator.INFINITE);
        rotateAnimation.setDuration(800);
        rotateAnimation.start();
    }
}
